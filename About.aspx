﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Portfolio.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Tools</h2>
        <asp:Panel runat="server" GroupingText="Languages & Libraries" BorderStyle="Groove" BorderColor="darkgray" style="margin-bottom: 10px">
            <asp:Image runat="server" CssClass="img-Logos" ImageUrl="Images/CSharp.png" ToolTip="C Sharp"/>
        <asp:Image runat="server" ImageUrl="Images/ASP.png" CssClass="img-Logos" ToolTip="ASP.net"/>
        <asp:Image runat="server" ImageUrl="Images/JS.png" CssClass="img-Logos" ToolTip="Javascript"/>
        <asp:Image runat="server" ImageUrl="Images/html.png" CssClass="img-Logos" ToolTip="HTML"/>
    </asp:Panel>
        <asp:Panel runat="server" GroupingText="Database Tools" BorderStyle="Groove" BorderColor="darkgray" style="margin-bottom: 10px">
            <asp:Image runat="server" ImageUrl="Images/sql.png" CssClass="img-Logos" ToolTip="SSMS"/>
        </asp:Panel>
        <asp:Panel runat="server" GroupingText="Management Tools" BorderStyle="Groove" BorderColor="darkgray" style="margin-bottom: 10px">
            <asp:Image runat="server" ImageUrl="Images/BitBucket.png" CssClass="img-Logos" ToolTip="BitBucket"/>
            <asp:Image runat="server" ImageUrl="Images/jira.png" CssClass="img-Logos" ToolTip="Jira"/>
            <asp:Image runat="server" ImageUrl="Images/Azure.png" CssClass="img-Logos" ToolTip="Azure"/>
            <asp:Image runat="server" ImageUrl="Images/git.png" CssClass="img-Logos" ToolTip="Git"/>
        </asp:Panel>
</asp:Content>
