﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Portfolio
{
    public partial class School : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BuildHashtable();
            }
            
        }

        protected void BuildHashtable()
        {
            Hashtable ht = new Hashtable();

            ht.Add("CIS115: Logic and Design",
                "This course introduces basics of programming logic, as well as algorithm design and development, including constants, variables, expressions, arrays, files and control structures for sequential, iterative and decision processing. Students learn to design and document program specifications using tools such as flowcharts, structure charts and pseudocode. Program specification validation through desk-checking and walk-throughs is also covered.");
            ht.Add("CIS170C: Programming with Lab",
                "This course introduces basics of coding programs from program specifications, including use of an integrated development environment(IDE), language syntax, as well as debugger tools and techniques.Students also learn to develop programs that manipulate simple data structures such as arrays, as well as different types of files.C++.Net is the primary programming language used. ");
            ht.Add("CIS247C: Object-Oriented Programming with Lab",
                "This course introduces object-oriented programming concepts including objects, classes, encapsulation, polymorphism and inheritance. Using an object-oriented programming language students design, code, test and document business-oriented programs. C++.Net is the primary programming language used.");
            ht.Add("CIS206: Architecture and Operating Systems with Lab",
                "This course introduces operating system concepts by examining various operating systems such as Windows, UNIX and Linux. Students also study typical desktop system hardware, architecture and configuration.");
            ht.Add("CIS321: Structured Analysis and Design",
                "This course introduces the systems analysis and design process using information systems methodologies and techniques to analyze business activities and solve problems. Students learn to identify, define and document business problems and then develop information system models to solve them.");
            ht.Add("CIS336: Introduction to Database with Lab",
                "This course introduces concepts and methods fundamental to database development and use including data analysis and modeling, as well as structured query language (SQL). Students also explore basic functions and features of a database management system (DBMS), with emphasis on the relational model.");
            ht.Add("CIS339: Object-Oriented Analysis and Design",
                "Building on the foundation established in CIS321, students explore techniques, tools and methods used in the object-oriented approach to developing applications. Students learn how to model and design system requirements using tools such as Unified Modeling Language (UML)");
            ht.Add("CIS355A: Business Application Programming with Lab",
                "Building on analysis, programming and database skills developed in previous courses, this course introduces fundamental principles and concepts of developing programs that support typical business processing activities and needs such as transaction processing and report generation. Students develop business-oriented programs that deal with error handling, data validation and file handling. Java is the primary programming language used.");
            ht.Add("CIS363B: Web Interface Design with Lab",
                "This course introduces web design and basic programming techniques for developing effective and useful websites. Coursework emphasizes website structure and navigational models, practical and legal usability considerations, and performance factors related to using various types of media and tools such as hypertext markup language (HTML), cascading style sheets (CSS), dynamic HTML (DHTML) and scripting. Extensible");
            ht.Add("CIS407A: Web Application Development with Lab",
                "This course builds on analysis, interface design and programming skills learned in previous courses and introduces basics of design, coding and scripting, as well as database connectivity for web-based applications. A programming language such as Visual Basic.Net, C++.Net or C#.Net is used to implement web-based applications. ASP.Net is the primary software tool used.");

            rptClassList.DataSource = ht;
            rptClassList.DataBind();

        }


    }
}