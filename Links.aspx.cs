﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portfolio
{
    public partial class Links : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDownloadResume_OnClick(object sender, ImageClickEventArgs e)
        {
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=\"James_Willis_Resume.pdf\"");
            Response.BinaryWrite(System.IO.File.ReadAllBytes("C:/Users/Willi/source/repos/Portfolio/Files/James_Willis_Resume.pdf"));
            Response.Flush();
            Response.End();

        }
    }
}