﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Portfolio._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


        <h1>About me</h1>
        <asp:Image runat="server" ImageUrl="Images/IMG_0203[982].jpeg" Width="147px" Height="157px" Style="float:left; margin-right: 20px"/>
        <p class="lead">I moved to the US From Yorkshire, England in 2008. Since then I have lived in Oregon and California but finally found a home here in Pittsburgh Pennsylvania. </p>
        <p class="lead">I graduated from DeVry University in 2016 where I completed my degree in Computer Information Systems - Web Development</p>
        <p class="lead">My professional Career began as a Project Manager and later a Technical Business Analyst for PNC Bank. However, I my desire was always to be a developer and when the opportunity came I took it. </p>
        <p class="lead">My Developer career so far has been all full stack positions using predominantly C#/.Net and utilizing HTML, JS and CSS to create a range applications</p>
        <p class="lead">I look forward to continuing my career and learning as much as I can about the entire architecture of development to realize my dream of being a Technology Manager.</p>
        

</asp:Content>
