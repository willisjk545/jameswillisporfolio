﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="School.aspx.cs" Inherits="Portfolio.School" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <style>#more {display: none;}</style>
    <h2>School</h2>
    <asp:Repeater runat="server" id="rptClassList" >
        <ItemTemplate>

            <br/><span class="text-center;" style="font-size: 24px; font-weight: 200"><%#((DictionaryEntry)Container.DataItem).Key%></span>
            
            <label class="switch">
                <%--Make the ID of the checkbox = a string plus the current iteration number of the repeater and pass that iteration number in the client side method--%>
                <input type="checkbox" id="chkHideShow<%# (Container).ItemIndex%>" onchange='<%# "return HideShow(" + Container.ItemIndex + ")" %>'>
                <span class="slider round"></span>
            </label><br/>
        
            <span id="spnMoreInfo<%# (Container).ItemIndex%>" class="text-center" style="display: none"><%#((DictionaryEntry)Container.DataItem).Value%> <br/></span> 

        </ItemTemplate>
    </asp:Repeater>

    <script type="text/javascript">
        function HideShow(selectedIteration) {

            var checkbox = document.getElementById("chkHideShow" + selectedIteration).checked;
            var span = document.getElementById("spnMoreInfo" + selectedIteration);

            if (checkbox == false) {
                span.style.display = "none";
            } else {
                span.style.display = "inline";
            }

        }
    </script>
</asp:Content>


