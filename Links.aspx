﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Links.aspx.cs" Inherits="Portfolio.Links" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:Panel runat="server" GroupingText="Links" BorderStyle="Groove" BorderColor="darkgray" style="margin-bottom: 10px">
            <a href="https://www.linkedin.com/in/james-willis-302618126" target="_blank">
            <asp:Image runat="server" ImageUrl="Images/linkedin.png" CssClass="img-Logos" ToolTip="LinkedIn"/></a>
    </asp:Panel>
    
    <asp:Panel runat="server" GroupingText="Downloads" BorderStyle="Groove" BorderColor="darkgray" style="margin-bottom: 10px">
        <asp:ImageButton ID="btnDownloadResume" runat="server" ImageUrl="Images/resumelogo.png" CssClass="img-Logos" ToolTip="Resume" OnClick="btnDownloadResume_OnClick"/>
    </asp:Panel>

</asp:Content>
