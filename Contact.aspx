﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Portfolio.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Contact</h2>
    <address>
        2212 Superior St<br />
        Bethel Park, PA 15102<br />
        Phone: (541) 232-1126<br />
    </address>

    <address>
        <strong>Email:</strong>   <a href="mailto:willisjk545@gmail.com">willisjk545@gmail.com</a><br />
    </address>
</asp:Content>
